//CREANDO SCRIPT PARA CONSUMIR SERVICIO MEDIANTE AXIOS
//BOTÓN MOSTRAR DONDE SE UTILIZA AXIOS PARA VER LA INFORMACIÓN QUE ESTA EN EL ARCHIVO alumnos.json
document.getElementById("btnMostrar").addEventListener("click", function() {
    axios
    .get("/html/alumnos.json")
    .then(function(response) {
        const alumnos = response.data;
        const alumnosBody = document.getElementById("alumnosBody");
        let promTotal = 0;
        for (let i = 0; i < alumnos.length; i++) {
          const alumno = alumnos[i];
          const prom = (alumno.matematicas + alumno.quimica + alumno.fisica + alumno.geografia) / 4;
          promTotal += prom;
          alumnosBody.innerHTML += `
            <tr>
              <td>${alumno.id}</td>
              <td>${alumno.matricula}</td>
              <td>${alumno.nombre}</td>
              <td>${alumno.matematicas}</td>
              <td>${alumno.quimica}</td>
              <td>${alumno.fisica}</td>
              <td>${alumno.geografia}</td>
              <td>${prom}</td>
            </tr>
          `;
        }
        promTotal /= alumnos.length;
        document.getElementById("promGeneral").innerHTML = "Promedio general: " + promTotal.toFixed(2);
        document.getElementById("tablaAlumnos").style.display = "table";
    })
    .catch(function(error) {
        console.error(error);
    });
});

//BOTÓN PARA LIMPIAR LA INFORMACIÓN
document.getElementById("btnLimpiar").addEventListener("click", function() {
    document.getElementById("alumnosBody").innerHTML = "";
    document.getElementById("promGeneral").innerHTML = "";
    document.getElementById("tablaAlumnos").style.display = "none";
});